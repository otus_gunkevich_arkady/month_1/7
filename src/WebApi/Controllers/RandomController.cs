using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System;
using WebApi.Models;
using System.Linq;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class RandomController : Controller
    {
        private List<Customer> _customers;
        DataBaseContext _context;
        public RandomController(DataBaseContext context)
        {
            _context = context;
            _customers = new List<Customer>();
            using (var reader = new StreamReader(@"Data\FIO.csv"))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine().Split(';');
                    _customers.Add(new Customer() { Firstname = line[0], Surname = line[1], Lastname = line[2] });
                }
            }
        }
        /// <summary>
        /// Получить случайносгенерированного Customer
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<Customer>> Get()
        {
            try
            {
                Random _rnd = new Random();
                int _max = _customers.Count();
                return Ok(new Customer()
                {
                    Firstname = _customers[_rnd.Next(1, _max)].Firstname,
                    Surname = _customers[_rnd.Next(1, _max)].Surname,
                    Lastname = _customers[_rnd.Next(1, _max)].Lastname
                });
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Создадим какое то количество случайных клиентов
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> Post()
        {
            try
            {
                Random _rnd = new Random();
                List<Customer> _newCustomers = new List<Customer>();
                using (var reader = new StreamReader(@"Data\FIO.csv"))
                {
                    while (!reader.EndOfStream)
                    {
                        var line = reader.ReadLine().Split(';');
                        //Добавим только часть (не всех)
                        if (_rnd.Next(1, 100) < 10)
                            _newCustomers.Add(new Customer() { Firstname = line[0], Surname = line[1], Lastname = line[2] });
                    }
                }
                await _context.Customers.AddRangeAsync(_newCustomers);
                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}
