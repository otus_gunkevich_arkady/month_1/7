﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace WebClient.Services
{
    public class MenuService : IMenuService
    {
        private Menu _menu;
        private Stack<MenuPage> _stackMenuPage;
        private int _pageList = 1;
        private int _maxPageList = 10;
        private int _customersMaxCount = 0;
        private string _messageLoad = "......идёт загрузка данных......";
        private string _messageInfo = "";
        private ICustomerService _customerService;
        private IRandomService _randomService;
        public delegate void DelegateExit();
        public event DelegateExit HandleExit;
        public MenuService()
        {
            _stackMenuPage = new Stack<MenuPage>();
            _stackMenuPage.Push(MenuPage.Default);
            _customerService = new CustomerService();
            _randomService = new RandomService();
            LoadMenu().GetAwaiter();
        }
        public async Task Enter() => await _menu.ExecuteButton();
        public async Task Up() => _menu.UpSelect();
        public async Task Down() => _menu.DownSelect();
        public async Task Left()
        {
            _pageList -= (_pageList > 1 ? 1 : 0);
            await LoadMenu();
        }
        public async Task Right()
        {
            _pageList += (_pageList * _maxPageList <= _customersMaxCount ? 1 : 0);
            await LoadMenu();
        }
        public void Write() => _menu.Write();
        private async Task LoadMenu()
        {
            Console.WriteLine(_messageLoad);
            var _page = _stackMenuPage.Peek();
            List<MenuButton> _buttons = new List<MenuButton>();
            List<string> _topInfo = new List<string>();
            List<string> _footerInfo = new List<string>();
            _topInfo.Add(string.Join('/', _stackMenuPage.Reverse().Select(s => DisplayName(s)).ToArray()));


            switch (_page)
            {
                case MenuPage.Default:
                    {
                        _buttons.Add(new MenuButton("(Клиенты) Управление", async () => { _stackMenuPage.Push(MenuPage.Customers); await LoadMenu(); }));
                        _footerInfo.Add("");
                        _footerInfo.Add("Гункевич Аркадий Игоревич | OTUS");
                        _footerInfo.Add("ДЗ: Добавляем взаимодействие между клиентом и сервером");
                        break;
                    }
                case MenuPage.Customers:
                    {
                        _buttons.Add(new MenuButton("Вывести всех клиентов", async () => { _pageList = 1; await PushMenu(MenuPage.CustomersGet); }));
                        _buttons.Add(new MenuButton("Вывести клиента по ID", async () => { await PushMenu(MenuPage.CustomersGetById); }));
                        _buttons.Add(new MenuButton("Удалить всех клиентов", async () => { await PushMenu(MenuPage.CustomersDelete); }));
                        _buttons.Add(new MenuButton("Удалить клиета по ID", async () => { await PushMenu(MenuPage.CustomersDeleteById); }));
                        _buttons.Add(new MenuButton("Создать клиента", async () => { await PushMenu(MenuPage.CustomerCreate); }));
                        _buttons.Add(new MenuButton("* Добавить в БД много разных клиентов", async () => { await PushMenu(MenuPage.CustomersRandomCreate); }));
                        break;
                    }
                case MenuPage.CustomersGet:
                    {
                        var _customers = await _customerService.GetAsync((_pageList - 1) * _maxPageList, _maxPageList);
                        if (_customersMaxCount < 1) _customersMaxCount = _customerService.GetAsync(0, 0).Result.Count();
                        if (_customers is null)
                        {
                            _footerInfo.Add("Произошла ошибка");
                        }
                        else
                        {
                            _customers.ToList().ForEach(s => { _footerInfo.Add(s.GetInfo); });
                            _footerInfo.Add($"{(_pageList > 1 ? "<<" : "[")} {_pageList}/{_customersMaxCount / _maxPageList + 1} {(_pageList * _maxPageList <= _customersMaxCount ? ">>" : "]")}");
                        }
                        break;
                    }
                case MenuPage.CustomersGetById:
                    {
                        _buttons.Add(new MenuButton("Вывести клиента по ID", async () => { await CustomerGetById(); }));
                        break;
                    }
                case MenuPage.CustomersDelete:
                    {
                        _topInfo.Add("Удалить все записи из таблицы клиентов?");
                        _buttons.Add(new MenuButton("Да (удалить всех клиентов)", async () =>
                        {
                            var _deleteCustomers = await _customerService.DeleteAsync();
                            if (!_deleteCustomers)
                            {
                                Console.WriteLine("Произошла ошибка.");
                            }
                            else
                            {
                                _footerInfo.Add("Все записи из таблицы клиентов были удалены.");
                                _customersMaxCount = _customerService.GetAsync(0, 0).Result.Count();
                            }
                        }));
                        break;
                    }
                case MenuPage.CustomersDeleteById:
                    {
                        _buttons.Add(new MenuButton("Удалить клиета по ID", async () => { await CustomerDeleteById(); }));
                        break;
                    }
                case MenuPage.CustomersRandomCreate:
                    {
                        _topInfo.Add("Добавить в таблицу случайных клиентов?");
                        _buttons.Add(new MenuButton("Да", async () =>
                        {
                            var _createCustomers = await _randomService.PostAsync();
                            if (!_createCustomers)
                            {
                                Console.WriteLine("Произошла ошибка.");
                            }
                            else
                            {
                                _footerInfo.Add("В базу добавлено случайное количество случайных клиентов.");
                            }
                        }));
                        _buttons.Add(new MenuButton("Нет", async () => { await DownMenu(); }));
                        break;
                    }
                case MenuPage.CustomerCreate:
                    {
                        _buttons.Add(new MenuButton("Создать клиента самостоятельно.", async () => { await CustomerCreate(); }));
                        _buttons.Add(new MenuButton("Создать клиента случайным образом на сервере.", async () => { await PushMenu(MenuPage.CustomerCreateRandom); }));
                        break;
                    }
                case MenuPage.CustomerCreateRandom:
                    {
                        var _randomCustomer = await _randomService.GetAsync();
                        _buttons.Add(new MenuButton("Сгенерировать нового клиента", async () => { await LoadMenu(); }));
                        _buttons.Add(new MenuButton("Подвердить создание клиента", async () =>
                        {
                            var _client = await _customerService.PostAsync(_randomCustomer);
                            if (_client is null)
                            {
                                _footerInfo.Add("Что то пошло не так!");
                            }
                            else
                            {
                                _buttons = new List<MenuButton>();
                                _messageInfo = $"Клиент успешно создан.\n{_client.GetInfo}";
                                await DownMenu();
                            }
                        }));
                        _footerInfo.Add($"Случайны пользователь: {_randomCustomer.GetInfo}");
                        break;
                    }
                default: break;
            }

            if (_page != MenuPage.Default)
                _buttons.Add(new MenuButton("[*] Назад", async () => { await DownMenu(); }));
            _buttons.Add(new MenuButton("[*] Закрыть приложение", async () => { HandleExit.Invoke(); }));
            _footerInfo.Add(_messageInfo);

            _menu = new Menu(_buttons, _topInfo, _footerInfo);
            _messageInfo = "";
        }
        private async Task PushMenu(MenuPage menuPage)
        {
            if (!_stackMenuPage.Contains(menuPage))
                _stackMenuPage.Push(menuPage);
            await LoadMenu();
        }
        private async Task DownMenu()
        {
            var _deleteMenuPage = _stackMenuPage.Pop();
            await LoadMenu();
        }

        #region CustomerById
        private async Task CustomerDeleteById()
        {
            Console.Clear();
            Console.WriteLine("Введите -1 что бы вернуться в предидущее меню.");
            while (true)
            {
                Console.Write("Введите ID:");
                string _inputId = Console.ReadLine();
                var _success = long.TryParse(_inputId, out var _idClient);
                if (_success)
                {
                    if (_idClient == -1)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine(_messageLoad);
                        var _client = await _customerService.GetByIdAsync(_idClient);
                        if (_client is null)
                        {
                            Console.WriteLine("Такого клиента нет.");
                        }
                        else
                        {
                            var _isClientDelete = await _customerService.DeleteByIdAsync(_client.Id);
                            if (_isClientDelete)
                            {
                                _messageInfo = "Клиент успешно удалён";
                                await PushMenu(MenuPage.CustomersDeleteById);
                                break;
                            }
                            else
                                _messageInfo = "Что то пошло не так!";
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Неккоректный ввод.");
                }
            }
        }
        private async Task CustomerGetById()
        {
            Console.Clear();
            Console.WriteLine("Введите -1 что бы выйти.");
            while (true)
            {
                Console.Write("Введите ID:");
                string _inputId = Console.ReadLine();
                var _success = long.TryParse(_inputId, out var _idClient);
                if (_success)
                {
                    if (_idClient == -1)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine(_messageLoad);
                        var _client = await _customerService.GetByIdAsync(_idClient);
                        if (_client is null)
                        {
                            Console.WriteLine("Такого клиента нет.");
                        }
                        else
                        {
                            _messageInfo = _client.GetInfo;
                            await PushMenu(MenuPage.CustomersGetById);
                            break;
                        }
                    }
                }
                else
                {
                    Console.WriteLine("Неккоректный ввод.");
                }
            }
        }
        private async Task CustomerCreate()
        {
            Console.Clear();
            Customer _newCustomer = new Customer();
            string _input = string.Empty;
            while (_newCustomer.Surname == string.Empty)
            {
                Console.Write("Введите Фамилию:");
                _input = Console.ReadLine();
                if (_input == "")
                    Console.WriteLine("Неккоректный ввод.");
                else
                    _newCustomer.Surname = _input;
            }
            while (_newCustomer.Firstname == string.Empty)
            {
                Console.Write("Введите Имя:");
                _input = Console.ReadLine();
                if (_input == "")
                    Console.WriteLine("Неккоректный ввод.");
                else
                    _newCustomer.Firstname = _input;
            }
            while (_newCustomer.Lastname == string.Empty)
            {
                Console.Write("Введите отчество:");
                _input = Console.ReadLine();
                if (_input == "")
                    Console.WriteLine("Неккоректный ввод.");
                else
                    _newCustomer.Lastname = _input;
            }
            while (true)
            {
                _newCustomer.Write();
                Console.Write("Создать клиента Д/Н:");
                _input = Console.ReadLine();
                if (_input.ToUpper() == "Y" || _input.ToUpper() == "Д")
                {
                    Console.WriteLine("Создаём клиента.");
                    var _client = await _customerService.PostAsync(_newCustomer);
                    if (_client is null)
                    {
                        _messageInfo = "Что то пошло не так!";
                    }
                    else
                    {
                        _messageInfo = $"Клиент успешно создан.\n{_client.GetInfo}";
                        await LoadMenu();
                        break;
                    }
                }
                else if (_input.ToUpper() == "N" || _input.ToUpper() == "Н")
                {
                    await DownMenu();
                    break;
                }
            }
        }
        #endregion

        #region Enum
        //Немного воровоной рефлексии
        //https://josipmisko.com/posts/string-enums-in-c-sharp-everything-you-need-to-know
        private static readonly ConcurrentDictionary<string, string> DisplayNameCache = new ConcurrentDictionary<string, string>();
        private static string DisplayName(MenuPage value)
        {
            var key = $"{value.GetType().FullName}.{value}";

            var displayName = DisplayNameCache.GetOrAdd(key, x =>
            {
                var name = (DescriptionAttribute[])value
                    .GetType()
                    .GetTypeInfo()
                    .GetField(value.ToString())
                    .GetCustomAttributes(typeof(DescriptionAttribute), false);

                return name.Length > 0 ? name[0].Description : value.ToString();
            });

            return displayName;
        }
        private enum MenuPage
        {
            [Description("Основное меню")]
            Default,
            [Description("Клиенты")]
            Customers,
            [Description("Вывод клиентов")]
            CustomersGet,
            [Description("Вывод клиента по ID")]
            CustomersGetById,
            [Description("Удаление клиентов")]
            CustomersDelete,
            [Description("Удаление клиента по ID")]
            CustomersDeleteById,
            [Description("Создание клиента")]
            CustomerCreate,
            [Description("Создание случайного клиента")]
            CustomerCreateRandom,
            [Description("Наполнение таблицы клиентов")]
            CustomersRandomCreate
        }
        #endregion
    }
}
