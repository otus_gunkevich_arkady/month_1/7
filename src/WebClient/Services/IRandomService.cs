﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebClient.Services
{
    public interface IRandomService
    {
        Task<Customer> GetAsync();
        Task<bool> PostAsync();
    }
}
