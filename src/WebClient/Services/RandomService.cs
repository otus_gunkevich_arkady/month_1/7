﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WebClient.Services
{
    public class RandomService : IRandomService
    {
        private readonly HttpClient _httpClient;
        public RandomService()
        {
            _httpClient = new HttpClient();
        }

        public async Task<Customer> GetAsync()
        {
            try
            {
                return await _httpClient.GetFromJsonAsync<Customer>($"{SettingAPI.BASIC_ADRESS_API}/{SettingAPI.CONTROLLER_RANDOM}");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<bool> PostAsync()
        {
            try
            {
                var response = await _httpClient.PostAsJsonAsync($"{SettingAPI.BASIC_ADRESS_API}/{SettingAPI.CONTROLLER_RANDOM}", new { });
                if (!response.IsSuccessStatusCode)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
