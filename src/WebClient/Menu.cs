﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebClient.Services;

namespace WebClient
{
    public class Menu
    {
        private List<MenuButton> _buttons { get; set; }
        private List<string> _topInfo { get; set; }
        private List<string> _footerInfo { get; set; }
        public Menu(List<MenuButton> buttons, List<string> topInfo, List<string> footerInfo)
        {
            _buttons = buttons;
            buttons.First().IsSelect = true;
            _topInfo = topInfo;
            _footerInfo = footerInfo;
        }
        public void Write()
        {
            Console.Clear();
            _topInfo.ForEach(s => Console.WriteLine(s));
            Console.WriteLine("====================================");
            _buttons.ForEach(s =>
            {
                if (s.IsSelect)
                {
                    Console.BackgroundColor = ConsoleColor.DarkGray;
                    Console.WriteLine($"{s.Info}");
                }
                else
                {
                    Console.WriteLine($"{s.Info}");
                }
                Console.BackgroundColor = ConsoleColor.Black;
            });
            Console.WriteLine("====================================");
            _footerInfo.ForEach(s => Console.WriteLine(s));
        }
        public async Task ExecuteButton() => await _buttons.SingleOrDefault(s => s.IsSelect).Execute();
        public void UpSelect()
        {
            int _changeIndex = _buttons.IndexOf(_buttons.SingleOrDefault(s => s.IsSelect));
            if (_changeIndex > 0)
            {
                _buttons[_changeIndex - 1].IsSelect = true;
                _buttons[_changeIndex].IsSelect = false;
            }
        }
        public void DownSelect()
        {
            int _changeIndex = _buttons.IndexOf(_buttons.SingleOrDefault(s => s.IsSelect));
            if (_changeIndex < _buttons.Count() - 1)
            {
                _buttons[_changeIndex + 1].IsSelect = true;
                _buttons[_changeIndex].IsSelect = false;
            }
        }
    }
    public class MenuButton
    {
        public bool IsSelect { get; set; }
        public string Info { get; private set; }
        public Func<Task> Execute { get; private set; }
        public MenuButton(string info, Func<Task> execute = null)
        {
            IsSelect = false;
            Info = info;
            Execute = (execute is null ? async () => { } : execute);
        }
    }
}
