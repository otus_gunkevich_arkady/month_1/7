using System.ComponentModel.DataAnnotations;

namespace WebApi.Models
{
    public class Customer
    {
        [Key]
        public long Id { get; set; }
        // C# 9.0 драсти
        // Последнее я читал C# 6.0
        [Required]
        public string Firstname { get; set; }
        //Нет, ну мы же в РФ, у нас тут ФИО
        [Required]
        public string Surname { get; set; }
        [Required]
        public string Lastname { get; set; }

    }
}