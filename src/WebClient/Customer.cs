using System;
using System.ComponentModel.DataAnnotations.Schema;
using WebClient.Services;

namespace WebClient
{
    public class Customer
    {
        public long Id { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Lastname { get; set; }
        public Customer()
        {
            Id = 0;
            Firstname = string.Empty;
            Surname = string.Empty;
            Lastname = string.Empty;
        }
        public void Write() => Console.WriteLine($"{Surname} {Firstname} {Lastname}");
        public string GetInfo => $"[Id = {Id}] {Surname} {Firstname} {Lastname}";
    }
}