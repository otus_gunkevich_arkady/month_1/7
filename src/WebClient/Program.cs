﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using WebClient.Services;

namespace WebClient
{

    static class Program
    {
        public static void ExitApp() => Environment.Exit(0);
        public static async Task Main(string[] args)
        {
            var _newMenuService = new MenuService();
            _newMenuService.HandleExit += ExitApp;
            IMenuService _menuService = _newMenuService;

            while (true)
            {
                _menuService.Write();

                var _key = Console.ReadKey();
                switch (_key.Key)
                {
                    case ConsoleKey.UpArrow: { await _menuService.Up(); break; }
                    case ConsoleKey.DownArrow: { await _menuService.Down(); break; }
                    case ConsoleKey.LeftArrow: { await _menuService.Left(); break; }
                    case ConsoleKey.RightArrow: { await _menuService.Right(); break; }
                    case ConsoleKey.Enter: { await _menuService.Enter(); break; }
                    default: { break; }
                }
            }
        }
    }
}