﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebClient.Services
{
    public interface ICustomerService
    {
        Task<List<Customer>> GetAsync(int skip, int take);
        Task<Customer> GetByIdAsync(long id);
        Task<Customer> PostAsync(Customer customer);
        Task<Customer> PutAsync(Customer customer);
        Task<bool> DeleteByIdAsync(long id);
        Task<bool> DeleteAsync();
    }
}
