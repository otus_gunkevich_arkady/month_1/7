﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace WebClient.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly HttpClient _httpClient;
        public CustomerService()
        {
            _httpClient = new HttpClient();
        }
        public async Task<List<Customer>> GetAsync(int skip = 0, int take = 0)
        {
            try
            {
                if (take != 0) return await _httpClient.GetFromJsonAsync<List<Customer>>($"{SettingAPI.BASIC_ADRESS_API}/{SettingAPI.CONTROLLER_CUSTOMER}?skip={skip}&take={take}");
                return await _httpClient.GetFromJsonAsync<List<Customer>>($"{SettingAPI.BASIC_ADRESS_API}/{SettingAPI.CONTROLLER_CUSTOMER}");
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public async Task<Customer> GetByIdAsync(long id)
        {
            try
            {
                return await _httpClient.GetFromJsonAsync<Customer>($"{SettingAPI.BASIC_ADRESS_API}/{SettingAPI.CONTROLLER_CUSTOMER}/{id}");
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public async Task<Customer> PostAsync(Customer customer)
        {
            try
            {
                var response = await _httpClient.PostAsJsonAsync($"{SettingAPI.BASIC_ADRESS_API}/{SettingAPI.CONTROLLER_CUSTOMER}", customer);
                if (!response.IsSuccessStatusCode)
                {
                    return null;
                }
                return await response.Content.ReadFromJsonAsync<Customer>();
            }
            catch
            {
                return null;
            }
        }
        public async Task<Customer> PutAsync(Customer customer)
        {
            return null;
        }
        public async Task<bool> DeleteByIdAsync(long id)
        {
            try
            {
                var response = await _httpClient.DeleteAsync($"{SettingAPI.BASIC_ADRESS_API}/{SettingAPI.CONTROLLER_CUSTOMER}/{id}");
                if (!response.IsSuccessStatusCode)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public async Task<bool> DeleteAsync()
        {
            try
            {
                var response = await _httpClient.DeleteAsync($"{SettingAPI.BASIC_ADRESS_API}/{SettingAPI.CONTROLLER_CUSTOMER}");
                if (!response.IsSuccessStatusCode)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
