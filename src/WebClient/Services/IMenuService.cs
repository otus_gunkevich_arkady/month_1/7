﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebClient.Services
{
    public interface IMenuService
    {
        void Write();
        Task Up();
        Task Down();
        Task Left();
        Task Right();
        Task Enter();
    }
}
