﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebClient
{
    public class SettingAPI
    {
        public static string BASIC_ADRESS = "http://localhost:5000";
        public static string BASIC_ADRESS_API = $"{BASIC_ADRESS}/api";
        public static string CONTROLLER_CUSTOMER = "Customer";
        public static string CONTROLLER_RANDOM = "Random";
    }
}
