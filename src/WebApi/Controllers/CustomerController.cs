using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApi.Models;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        DataBaseContext _context;
        public CustomerController(DataBaseContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Получить все строки из таблицы Customers
        /// </summary>
        /// <returns>List<Customer></returns>
        [HttpGet]
        public async Task<ActionResult<List<Customer>>> Get(int skip, int take)
        {
            try
            {
                if (take == 0) { return Ok(await _context.Customers.ToListAsync()); }
                return Ok(await _context.Customers.Skip(skip).Take(take).ToListAsync());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Получить запись из таблицы Customers по ID
        /// </summary>
        /// <param name="id">long</param>
        /// <returns>Customer</returns>
        [HttpGet("{id:long}")]
        public async Task<ActionResult<Customer>> Get(long id)
        {
            try
            {
                var _customer = await _context.Customers.FindAsync(id);
                if (_customer is null) return NotFound();
                return Ok(_customer);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Добавить новую запись в таблицу Customers
        /// </summary>
        /// <param name="customer">Customer</param>
        /// <returns>Customer</returns>
        [HttpPost]
        public async Task<ActionResult<Customer>> Post([FromBody]Customer customer)
        {
            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                //Вот тут он будет возвращать 409 ошибку, он же конфликт
                //Но по Id стоит идентификатор, так что он сам проставляет Id
                //Но возможность возврата 409 есть!
                if (await _context.Customers.AnyAsync(s => s.Id == customer.Id)) return Conflict();
                var _createCustomer = await _context.Customers.AddAsync(new Customer { Firstname = customer.Firstname, Surname = customer.Surname, Lastname = customer.Lastname });
                await _context.SaveChangesAsync();

                return Ok(_createCustomer.Entity);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        //REST Full ,   GET, POST, PUT, DELETE
        /// <summary>
        /// Изменить запись в таблице Customers
        /// </summary>
        /// <param name="customer">Customer</param>
        /// <returns>Customer</returns>
        [HttpPut]
        public async Task<ActionResult<Customer>> Put(Customer customer)
        {

            try
            {
                if (!ModelState.IsValid) return BadRequest(ModelState);
                if (!await _context.Customers.AnyAsync(s => s.Id == customer.Id)) return NotFound();
                var _customerReturn = _context.Customers.Update(customer);
                await _context.SaveChangesAsync();
                return Ok(_customerReturn.Entity);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Удалить запись из таблицы Customers
        /// </summary>
        /// <param name="id">long</param>
        [HttpDelete("{id:long}")]
        public async Task<ActionResult> Delete(long id)
        {
            try
            {
                var _customerDelete = await _context.Customers.FindAsync(id);
                if (_customerDelete is null) return NotFound();
                _context.Customers.Remove(_customerDelete);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        /// <summary>
        /// Удалить все записи из таблицы Customers
        /// Так делать нельзя, да и удаление по ID тоже делать нельзя
        /// Если это и правда таблица клиентов, тут надо иметь флаг IsDelete
        /// и на флаг проставлять true, но не удалять
        /// (((Но у нас обучение, поэтому, почему бы и нет)))
        /// </summary>
        /// <param name="id">long</param>
        [HttpDelete]
        public async Task<ActionResult> Delete()
        {
            try
            {
                var _customersDelete = await _context.Customers.ToListAsync();
                foreach (var _customerDelete in _customersDelete)
                    _context.Remove(_customerDelete);
                //Надо скинуть счётчик идентификатора
                await _context.Database.ExecuteSqlRawAsync("DBCC CHECKIDENT('Customers',RESEED,0);");
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }

}